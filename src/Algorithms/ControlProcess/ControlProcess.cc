#include "ControlProcess.h"
// ------------------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ------------------------------------------------------------------------------------------



ControlProcess::ControlProcess(uniset::ObjectId id, xmlNode* cnode):
	ControlProcess_SK(id, cnode),
	CheckWatherLevelTimer(uniset_conf()->getIntProp(cnode, "overrun_delay_msec"))
{	
	cout << "ControlProcess" << endl; 
	IncrementWatherLevel = 2; 

	if( cnode == NULL ) 
		throw Exception( myname + ": FAILED! not found confnode in confile!" );

	if( CheckWatherLevelTimer < 0 )
	    CheckWatherLevelTimer = 0;
}
// ------------------------------------------------------------------------------------------
ControlProcess::~ControlProcess()
{
}
// ------------------------------------------------------------------------------------------
ControlProcess::ControlProcess()
{
	cout << myname << ": init failed!!!!!!!!!!!!!!!"<< endl;
	throw Exception();
}
// ------------------------------------------------------------------------------------------
void ControlProcess::sensorInfo( const SensorMessage *sm )   
{
	std::cout << "sensorInfo function " << sm->id << endl; 
	if (sm->id == WatherLevel)   
	{
		if (in_WatherLevel){   
			askTimer(tmSensorInfo, CheckWatherLevelTimer, -1);   
		}
		else
			askTimer(tmSensorInfo, 0, -1);
	}

	if (sm->id == WatherLevel)    // WatherLevel
	{
		out_IsPumpOut= (in_WatherLevel > 90);  
		cout << out_IsPumpOut << endl; 
		out_IsPumpOut = (in_WatherLevel< 10);
	}
}
// ------------------------------------------------------------------------------------------
void ControlProcess::timerInfo( const TimerMessage *tm )
{
	if( tm->id == tmSensorInfo )
	{
		out_WatherLevelEmulator += IncrementWatherLevel;
		std::cout << out_WatherLevelEmulator << endl; 
		if (out_WatherLevelEmulator > 100)
		{
			out_WatherLevelEmulator = 100;
			IncrementWatherLevel = -2;
		}
		else if (out_WatherLevelEmulator < 0)
		{
			out_WatherLevelEmulator = 0;
			IncrementWatherLevel = 2;
		}
	}
}
// ------------------------------------------------------------------------------------------
